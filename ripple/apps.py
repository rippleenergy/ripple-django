from django.apps import AppConfig


class RippleConfig(AppConfig):
    name = 'ripple'
